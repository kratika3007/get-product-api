Get-Product API
===============

Implementation of RestFul API which consumes product name and product category to filter the product
data.

Configurations:
-------------

-> Configuration of PostgreSQL in application.properties.
	spring.datasource.username = (Add username of your PostgreSQL Database)
	spring.datasource.password = (Add password of your PostgreSQL Database)

-> Default server port is 8080.It can be changed in application.properties.
	sever.port = 8080 (Can be changed)
 
How to Build
------------

 1. Right click on pom.xml and select Run As->Maven build.
 2. In configuration dialog, Enter Goals as: clean package
 3. Select Apply and then select Run

How to Run:
-----------

**Method 1** (Preferred)
 1. Open Project directory in File Explorer.
 2. Enter target folder.
 3. Click the file bar and enter "cmd" to open the Command Prompt in current folder
 4. Run the following command (version is as specified in the pom.xml)
    #java -jar getProduct-<version>.jar

**Method 2** (Will need to run separate instance of Eclipse for different services)

 1. Right click RegistrationApplication.java in the src folder in the Eclipse Project Explorer
 2. Select Run As->Java Application




