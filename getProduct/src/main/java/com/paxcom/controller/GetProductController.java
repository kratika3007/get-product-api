package com.paxcom.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.paxcom.model.GetProductModel;
import com.paxcom.service.GetProductService;

@RestController
@RequestMapping(path = "/paxcom")
public class GetProductController {
	
	@Autowired
	GetProductService getProductService;
	
	@GetMapping(path = "/getProducts")
	public List<GetProductModel> getProducts(@RequestParam(required = false) String productName , @RequestParam(required = false) List<String> category){
		return getProductService.getProducts(productName, category);
	}

}
