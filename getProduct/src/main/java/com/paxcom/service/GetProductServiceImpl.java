package com.paxcom.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.paxcom.exception.NoRecordFoundException;
import com.paxcom.model.GetProductModel;
import com.paxcom.repository.GetProductRepository;

@Service
public class GetProductServiceImpl implements GetProductService{
	
	@Autowired
	private GetProductRepository getProductRepository;

	@Override
	public List<GetProductModel> getProducts(String productName, List<String> category) {
		List<GetProductModel> getProductList = new ArrayList<>();
		if(productName == null && category == null) {
			// fetching all the data 
			getProductList = getProductRepository.findAll(); 
		}else if(productName != null  && category == null) {
			//fetching data by passing product name
			getProductList = getProductRepository.findProductByProductName(productName); 
		}else if (productName == null  && category != null) {
			// fetching data by passing category list
			getProductList = getProductRepository.findProductByCategoryList(category); 
		}else {
			//fetching data by passing both product name and category list 
				getProductList = getProductRepository.findByProductNameAndCategory(productName, category);
		}
		//to check whether the records are there or not
		if(getProductList.isEmpty() || getProductList == null) {
			throw new NoRecordFoundException();
		}
		return getProductList;
	}

}
