package com.paxcom.service;

import java.util.List;

import com.paxcom.model.GetProductModel;

public interface GetProductService {

	List<GetProductModel> getProducts(String productName, List<String> category);

}
