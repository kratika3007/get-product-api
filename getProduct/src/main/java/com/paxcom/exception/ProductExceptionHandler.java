package com.paxcom.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ProductExceptionHandler {
	 @ExceptionHandler(value = NoRecordFoundException.class)
	 public ResponseEntity<Object> exception(NoRecordFoundException exception){
		 return new ResponseEntity<>("Records not found", HttpStatus.NOT_FOUND);
	 }

}
