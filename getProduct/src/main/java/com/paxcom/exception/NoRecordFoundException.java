package com.paxcom.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NoRecordFoundException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;

	public NoRecordFoundException () {
		super();
	}

}
