package com.paxcom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.paxcom.service" , "com.paxcom.controller" , "com.paxcom.model" , "com.paxcom.repository" , "com.paxcom.exception"})
public class GetProductApplication {

	public static void main(String[] args) {
		SpringApplication.run(GetProductApplication.class, args);
	}

}
