package com.paxcom.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.paxcom.model.GetProductModel;
@Repository
@Transactional
@EnableJpaRepositories
public interface GetProductRepository extends JpaRepository<GetProductModel, Long>{
	
	@Query("select p from GetProductModel p where lower(p.productName) like lower(concat('%', :productName, '%')) and p.category in :category")
	public List<GetProductModel> findByProductNameAndCategory(@Param("productName") String product_name , @Param("category") List<String> category);

	@Query("select p from GetProductModel p where p.category in :category")
	public List<GetProductModel> findProductByCategoryList(@Param("category") List<String> category);
	
	@Query("select p from GetProductModel p where lower(p.productName) like lower(concat('%', :productName, '%'))")
	public List<GetProductModel> findProductByProductName(@Param("productName") String product_name);
	
	@Query("select p from GetProductModel p")
	public List<GetProductModel> findAll();
}
