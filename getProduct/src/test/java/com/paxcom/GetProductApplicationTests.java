package com.paxcom;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.paxcom.model.GetProductModel;
import com.paxcom.repository.GetProductRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
class GetProductApplicationTests {
	
	@Autowired
	GetProductRepository getProductRepository;

	@Test
	public void getProductTestCases() {
		
		//	Fetching data by using product name or there initials
		
		List<GetProductModel> productListByProductName = new ArrayList<GetProductModel>();
		GetProductModel product1 = new GetProductModel(1L, "Laptop", 10.0, 20, "C1");
		productListByProductName.add(product1);
		List<GetProductModel> getProductModelByProductName = getProductRepository.findProductByProductName("La");
		Assertions.assertEquals(productListByProductName.get(0).getPid(), getProductModelByProductName.get(0).getPid());
		Assertions.assertEquals(productListByProductName.get(0).getProductName(), getProductModelByProductName.get(0).getProductName());
		Assertions.assertEquals(productListByProductName.get(0).getPrice(), getProductModelByProductName.get(0).getPrice());
		Assertions.assertEquals(productListByProductName.get(0).getQuantity(), getProductModelByProductName.get(0).getQuantity());
		Assertions.assertEquals(productListByProductName.get(0).getCategory(), getProductModelByProductName.get(0).getCategory());
		
		
		//		Fetching data by using category list
		
		List<String> category1 = new ArrayList<String>();
		category1.add("C1");
		category1.add("C3");
		List<GetProductModel> productListByCategory = new ArrayList<GetProductModel>();
		GetProductModel product2 = new GetProductModel(1L, "Laptop", 10.0, 20, "C1");
		GetProductModel product3 = new GetProductModel(4L, "KeyPad", 10.0, 20, "C3");
		productListByCategory.add(product2);
		productListByCategory.add(product3);
		List<GetProductModel> getProductModelByCategory = getProductRepository.findProductByCategoryList(category1);
		Assertions.assertEquals(productListByCategory.get(0).getPid(), getProductModelByCategory.get(0).getPid());
		Assertions.assertEquals(productListByCategory.get(0).getProductName(), getProductModelByCategory.get(0).getProductName());
		Assertions.assertEquals(productListByCategory.get(0).getPrice(), getProductModelByCategory.get(0).getPrice());
		Assertions.assertEquals(productListByCategory.get(0).getQuantity(), getProductModelByCategory.get(0).getQuantity());
		Assertions.assertEquals(productListByCategory.get(0).getCategory(), getProductModelByCategory.get(0).getCategory());
		Assertions.assertEquals(productListByCategory.get(1).getPid(), getProductModelByCategory.get(1).getPid());
		Assertions.assertEquals(productListByCategory.get(1).getProductName(), getProductModelByCategory.get(1).getProductName());
		Assertions.assertEquals(productListByCategory.get(1).getPrice(), getProductModelByCategory.get(1).getPrice());
		Assertions.assertEquals(productListByCategory.get(1).getQuantity(), getProductModelByCategory.get(1).getQuantity());
		Assertions.assertEquals(productListByCategory.get(1).getCategory(), getProductModelByCategory.get(1).getCategory());
		
		
		//		Fetching data by using both product name and category list
		
		List<String> category2 = new ArrayList<String>();
		category2.add("C2");
		List<GetProductModel> productListByProductNameAndCategory = new ArrayList<GetProductModel>();
		GetProductModel product4 = new GetProductModel(2L, "Mobile", 10.0, 20, "C2");
		GetProductModel product5 = new GetProductModel(3L,"Mouse", 10.0, 20, "C2");
		productListByProductNameAndCategory.add(product4);
		productListByProductNameAndCategory.add(product5);
		List<GetProductModel> getProductModelByProductNameAndCategory = getProductRepository.findByProductNameAndCategory("mo", category2);
		Assertions.assertEquals(productListByProductNameAndCategory.get(0).getPid(), getProductModelByProductNameAndCategory.get(0).getPid());
		Assertions.assertEquals(productListByProductNameAndCategory.get(0).getProductName(), getProductModelByProductNameAndCategory.get(0).getProductName());
		Assertions.assertEquals(productListByProductNameAndCategory.get(0).getPrice(), getProductModelByProductNameAndCategory.get(0).getPrice());
		Assertions.assertEquals(productListByProductNameAndCategory.get(0).getQuantity(), getProductModelByProductNameAndCategory.get(0).getQuantity());
		Assertions.assertEquals(productListByProductNameAndCategory.get(0).getCategory(), getProductModelByProductNameAndCategory.get(0).getCategory());
		Assertions.assertEquals(productListByProductNameAndCategory.get(1).getPid(), getProductModelByProductNameAndCategory.get(1).getPid());
		Assertions.assertEquals(productListByProductNameAndCategory.get(1).getProductName(), getProductModelByProductNameAndCategory.get(1).getProductName());
		Assertions.assertEquals(productListByProductNameAndCategory.get(1).getPrice(), getProductModelByProductNameAndCategory.get(1).getPrice());
		Assertions.assertEquals(productListByProductNameAndCategory.get(1).getQuantity(), getProductModelByProductNameAndCategory.get(1).getQuantity());
		Assertions.assertEquals(productListByProductNameAndCategory.get(1).getCategory(), getProductModelByProductNameAndCategory.get(1).getCategory());
		
		//		Fetching all data 
		
		List<GetProductModel> productLists = new ArrayList<GetProductModel>();
		GetProductModel product6 = new GetProductModel(1L, "Laptop", 10.0, 20, "C1");
		GetProductModel product7 = new GetProductModel(2L, "Mobile", 10.0, 20, "C2");
		GetProductModel product8 = new GetProductModel(3L,"Mouse", 10.0, 20, "C2");
		GetProductModel product9 = new GetProductModel(4L, "KeyPad", 10.0, 20, "C3");
		productLists.add(product6);
		productLists.add(product7);
		productLists.add(product8);
		productLists.add(product9);
		List<GetProductModel> getAllProductModel = getProductRepository.findAll();
		Assertions.assertEquals(productLists.get(0).getPid(), getAllProductModel.get(0).getPid());
		Assertions.assertEquals(productLists.get(0).getProductName(), getAllProductModel.get(0).getProductName());
		Assertions.assertEquals(productLists.get(0).getPrice(), getAllProductModel.get(0).getPrice());
		Assertions.assertEquals(productLists.get(0).getQuantity(), getAllProductModel.get(0).getQuantity());
		Assertions.assertEquals(productLists.get(0).getCategory(), getAllProductModel.get(0).getCategory());
		Assertions.assertEquals(productLists.get(1).getPid(), getAllProductModel.get(1).getPid());
		Assertions.assertEquals(productLists.get(1).getProductName(), getAllProductModel.get(1).getProductName());
		Assertions.assertEquals(productLists.get(1).getPrice(), getAllProductModel.get(1).getPrice());
		Assertions.assertEquals(productLists.get(1).getQuantity(), getAllProductModel.get(1).getQuantity());
		Assertions.assertEquals(productLists.get(1).getCategory(), getAllProductModel.get(1).getCategory());
		Assertions.assertEquals(productLists.get(2).getPid(), getAllProductModel.get(2).getPid());
		Assertions.assertEquals(productLists.get(2).getProductName(), getAllProductModel.get(2).getProductName());
		Assertions.assertEquals(productLists.get(2).getPrice(), getAllProductModel.get(2).getPrice());
		Assertions.assertEquals(productLists.get(2).getQuantity(), getAllProductModel.get(2).getQuantity());
		Assertions.assertEquals(productLists.get(2).getCategory(), getAllProductModel.get(2).getCategory());
		Assertions.assertEquals(productLists.get(3).getPid(), getAllProductModel.get(3).getPid());
		Assertions.assertEquals(productLists.get(3).getProductName(), getAllProductModel.get(3).getProductName());
		Assertions.assertEquals(productLists.get(3).getPrice(), getAllProductModel.get(3).getPrice());
		Assertions.assertEquals(productLists.get(3).getQuantity(), getAllProductModel.get(3).getQuantity());
		Assertions.assertEquals(productLists.get(3).getCategory(), getAllProductModel.get(3).getCategory());
		
		
	}

}
